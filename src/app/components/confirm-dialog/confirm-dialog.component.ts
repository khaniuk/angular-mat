import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ConfirmDialogModel } from 'src/app/models/share/confirm-dialog.model';

@Component({
  selector: 'am-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogModel
  ) {}

  ngOnInit(): void {
    if (this.data !== undefined) {
      this.data.message = this.data.message
        ? this.data.message
        : 'Are you sure to delete?';
    }
  }
}
