import { CountryFormComponent } from './country-form/country-form.component';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

import { DialogModel } from 'src/app/models/share/dialog.model';
import { CountryModel } from './../../models/country.model';

import { ActionEnum } from 'src/app/models/share/action-enum';

import { CountryService } from 'src/app/services/country.service';
import { ConfirmDialogModel } from 'src/app/models/share/confirm-dialog.model';
import { ConfirmDialogComponent } from 'src/app/components/confirm-dialog/confirm-dialog.component';
import { Toast } from 'src/app/shared/toast';
import { MessageEnum, ToastStyleEnum } from 'src/app/shared/message';

@Component({
  selector: 'am-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss'],
})
export class CountryComponent implements OnInit, AfterViewInit {
  columns: Array<string>;
  countries; //: MatTableDataSource;

  //@ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild('countryPaginator', { read: MatPaginator })
  countryPaginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private countryService: CountryService
  ) {
    this.columns = [
      'num',
      'name',
      'alpha_code',
      // 'calling_codes',
      'capital',
      'region',
      // 'subregion',
      'population',
      // 'demonym',
      // 'area',
      // 'gini',
      // 'currencies',
      // 'languages',
      // 'flag',
      'status',
      'actions',
    ];
    this.countries = new MatTableDataSource<CountryModel>([]);
  }

  ngOnInit(): void {
    this.countryService
      .getCountriesJson()
      .subscribe((response: Array<CountryModel>) => {
        this.countries.data = response;
        // use not recommended
        sessionStorage.setItem('countries', JSON.stringify(response));
      });
  }

  ngAfterViewInit() {
    this.countries.paginator = this.countryPaginator;
    this.countries.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.countries.filter = filterValue.trim().toLowerCase();
  }

  clearFilter(): void {
    this.countries.filter = '';
  }

  getCountries() {
    this.countryService.getCountries().then((response: Array<CountryModel>) => {
      this.countries.data = response;
    });
  }

  addCountry(): void {
    const dialogData: DialogModel = {
      formType: ActionEnum.CREATE,
    };
    const dialogRef = this.dialog.open(CountryFormComponent, {
      disableClose: true,
      width: '500',
      data: dialogData,
    });
    dialogRef.afterClosed().subscribe((response: boolean) => {
      if (response) {
        this.getCountries();
      }
    });
  }

  updateCountry(rowData: CountryModel): void {
    const dialogData: DialogModel = {
      formType: ActionEnum.UPDATE,
      record: rowData,
    };
    const dialogRef = this.dialog.open(CountryFormComponent, {
      disableClose: true,
      width: '500',
      data: dialogData,
    });
    dialogRef.afterClosed().subscribe((response: boolean) => {
      if (response) {
        this.getCountries();
      }
    });
  }

  deleteCountry(rowData: CountryModel) {
    // console.log('delete', rowData);
    const confirmMessage: ConfirmDialogModel = {
      message: 'Are you sure to delete?',
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      disableClose: true,
      width: '350px',
      data: confirmMessage,
    });

    dialogRef.afterClosed().subscribe((respuesta: boolean) => {
      if (respuesta) {
        this.countryService
          .deleteCountry(rowData.id)
          .then((response: boolean) => {
            if (response) {
              dialogRef.close(response);
              Toast.open(
                this.snackBar,
                MessageEnum.MSG_DEL,
                ToastStyleEnum.SUCCESS
              );
              this.getCountries();
            } else {
              Toast.open(
                this.snackBar,
                MessageEnum.MSG_NOT_DEL,
                ToastStyleEnum.ERROR
              );
            }
          });
      }
    });
  }
}
