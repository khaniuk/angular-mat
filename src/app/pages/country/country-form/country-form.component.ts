import { Component, Inject, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CountryModel } from 'src/app/models/country.model';
import { ActionEnum } from 'src/app/models/share/action-enum';

import { DialogModel } from 'src/app/models/share/dialog.model';
import { CountryService } from 'src/app/services/country.service';
import { MessageEnum, ToastStyleEnum } from 'src/app/shared/message';
import { Toast } from 'src/app/shared/toast';

@Component({
  selector: 'am-country-form',
  templateUrl: './country-form.component.html',
  styleUrls: ['./country-form.component.scss'],
})
export class CountryFormComponent implements OnInit {
  form: FormGroup;

  maxDate: Date;

  btnLoading: boolean;
  regions: Array<any>;

  autoTicks = true;
  max = 100;
  min = 0;
  showTicks = false;
  step = 1;
  thumbLabel = true;
  value = 0;
  tickInterval = 1;

  getSliderTickInterval(): number | 'auto' {
    if (this.showTicks) {
      return this.autoTicks ? 'auto' : this.tickInterval;
    }

    return 0;
  }

  constructor(
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<CountryFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogModel,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private countryService: CountryService
  ) {
    this.form = new FormGroup({});
    this.maxDate = new Date();
    this.btnLoading = false;
    this.regions = [];
    this.getRegions();
  }

  ngOnInit(): void {
    if (this.data.formType === ActionEnum.CREATE) {
      this.form = this.formBuilder.group({
        id: [null],
        name: [
          null,
          [
            Validators.required,
            // Validators.pattern(ExpresionRegularEnum.Alfanumerico),
            Validators.minLength(2),
            Validators.maxLength(30),
          ],
        ],
        alpha2Code: [null, [Validators.required]],
        capital: [null, [Validators.required]],
        region: [null, [Validators.required]],
        status: [true, [Validators.required]],
        population: [null, [Validators.required]],
        area: [null],
        latlng: [null],
      });
    } else {
      const selectedRow: CountryModel = this.data.record;
      this.form = this.formBuilder.group({
        id: [selectedRow.id],
        name: [
          selectedRow.name,
          [
            Validators.required,
            // Validators.pattern(ExpresionRegularEnum.Alfanumerico),
            Validators.minLength(2),
            Validators.maxLength(30),
          ],
        ],
        alpha2Code: [selectedRow.alpha2Code, [Validators.required]],
        capital: [selectedRow.capital, [Validators.required]],
        region: [selectedRow.region, [Validators.required]],
        status: [selectedRow.status, [Validators.required]],
        population: [selectedRow.population, [Validators.required]],
        area: [selectedRow.area],
        latlng: [selectedRow.latlng],
      });
    }
  }

  getRegions() {
    this.regions = [
      {
        id: 'Americas',
        description: 'Americas',
      },
      {
        id: 'Asia',
        description: 'Asia',
      },
      {
        id: 'Africa',
        description: 'Africa',
      },
      {
        id: 'Oceania',
        description: 'Oceania',
      },
      {
        id: 'Europe',
        description: 'Europe',
      },
      {
        id: 'Polar',
        description: 'Polar',
      },
    ];
  }

  saveCountry() {
    if (this.form.valid) {
      this.btnLoading = true;
      const newRecord: CountryModel = {
        ...this.form.value,
      };

      if (this.data.formType === ActionEnum.CREATE) {
        this.countryService
          .saveCountry(newRecord)
          .then((response: boolean) => {
            this.btnLoading = false;
            if (response) {
              this.dialogRef.close(response);
              Toast.open(
                this.snackBar,
                MessageEnum.MSG_ADD,
                ToastStyleEnum.SUCCESS
              );
            } else {
              Toast.open(
                this.snackBar,
                MessageEnum.MSG_NOT_ADD,
                ToastStyleEnum.ERROR
              );
            }
          })
          .catch(() => {
            this.btnLoading = false;
            Toast.open(
              this.snackBar,
              MessageEnum.MSG_NOT_ADD,
              ToastStyleEnum.ERROR
            );
          });
      } else {
        this.countryService
          .putCountry(newRecord)
          .then((response: boolean) => {
            this.btnLoading = false;
            if (response) {
              this.dialogRef.close(response);
              Toast.open(
                this.snackBar,
                MessageEnum.MSG_UPD,
                ToastStyleEnum.SUCCESS
              );
            } else {
              Toast.open(
                this.snackBar,
                MessageEnum.MSG_NOT_UPD,
                ToastStyleEnum.ERROR
              );
            }
          })
          .catch(() => {
            this.btnLoading = false;
            Toast.open(
              this.snackBar,
              MessageEnum.MSG_NOT_UPD,
              ToastStyleEnum.ERROR
            );
          });
      }
    }
  }
}
