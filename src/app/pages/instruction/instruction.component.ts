import { Component, OnInit } from '@angular/core';

import { imageCode } from './../../utils/code-img';

@Component({
  selector: 'am-instruction',
  templateUrl: './instruction.component.html',
  styleUrls: ['./instruction.component.scss'],
})
export class InstructionComponent implements OnInit {
  components: Array<{ checked: boolean; description: string }>;
  image: string;
  constructor() {
    this.components = [
      { checked: false, description: 'Menu' },
      { checked: true, description: 'Tabs' },
      { checked: true, description: 'Table' },
      { checked: true, description: 'Paginator' },
      { checked: true, description: 'Search' },
      { checked: true, description: 'FormField' },
      { checked: true, description: 'Input' },
      { checked: true, description: 'Select' },
      { checked: true, description: 'Checkbox' },
      { checked: true, description: 'Button' },
      { checked: true, description: 'Dialog' },
    ];
    this.image = `data:image/webp;base64,${imageCode}`;
  }

  ngOnInit(): void {}
}
