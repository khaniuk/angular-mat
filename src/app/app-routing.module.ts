import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InstructionComponent } from './pages/instruction/instruction.component';
import { CountryComponent } from './pages/country/country.component';

const routes: Routes = [
  {
    path: 'instruction',
    component: InstructionComponent,
  },
  {
    path: 'country',
    component: CountryComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
