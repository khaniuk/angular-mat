import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, Observable, shareReplay } from 'rxjs';
import { MenuModel } from 'src/app/models/share/menu.model';

@Component({
  selector: 'am-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  menu: Array<MenuModel>;
  imageLogo: string;
  constructor(private breakpointObserver: BreakpointObserver) {
    this.menu = [];
    this.imageLogo = 'assets/logo.png';
  }

  sideBarOpen$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );

  ngOnInit(): void {
    this.menu = [
      {
        name: 'Instruction',
        link: '/instruction',
        icon: 'event_note',
      },
      {
        name: 'Country',
        link: '/country',
        icon: 'public',
      },
    ];
  }
}
