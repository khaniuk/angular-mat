import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';

import { InstructionComponent } from './pages/instruction/instruction.component';
import { CountryComponent } from './pages/country/country.component';
import { CountryFormComponent } from './pages/country/country-form/country-form.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';

import { OnlyNumberDirective } from './directives/only-number.directive';

@NgModule({
  declarations: [
    InstructionComponent,
    CountryComponent,
    CountryFormComponent,
    ConfirmDialogComponent,

    OnlyNumberDirective,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    MaterialModule,
  ],
})
export class ComponentModule {}
