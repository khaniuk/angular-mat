import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { CountryModel } from '../models/country.model';

@Injectable({
  providedIn: 'root',
})
export class CountryService {
  constructor(private http: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }

  getCountriesJson(): Observable<Array<CountryModel>> {
    return this.http
      .get<Array<CountryModel>>('./assets/api.json')
      .pipe(retry(), catchError(this.handleError));
  }

  async getCountries() {
    let countries: any = sessionStorage.getItem('countries');
    if (!countries) {
      const lala = await this.getCountriesJson().subscribe(
        (response: Array<CountryModel>) => {
          countries = response;
          sessionStorage.setItem('countries', JSON.stringify(response));
        }
      );
      console.log(lala);
    }
    let data: Array<CountryModel> = await JSON.parse(countries);
    return data;
  }

  async saveCountry(body: CountryModel) {
    let id: number = 1;
    let data: Array<CountryModel> = [];
    try {
      const countries = sessionStorage.getItem('countries');
      if (countries) {
        data = await JSON.parse(countries);
        id = data[data.length - 1].id + 1;
      }
      body.id = id;
      data.push(body);
      sessionStorage.setItem('countries', JSON.stringify(data));
      return true;
    } catch (error) {
      return false;
    }
  }

  async putCountry(body: CountryModel) {
    let position;
    let data: Array<CountryModel> = [];
    try {
      const countries = sessionStorage.getItem('countries');
      if (countries) {
        data = await JSON.parse(countries);
        position = data.findIndex((record) => record.id == body.id);
        if (position != -1) {
          data[position] = body;
        }
        sessionStorage.setItem('countries', JSON.stringify(data));
      }
      return true;
    } catch (error) {
      return false;
    }
  }

  async deleteCountry(id: number) {
    let position;
    let data: Array<CountryModel> = [];
    try {
      const countries = sessionStorage.getItem('countries');
      if (countries) {
        data = await JSON.parse(countries);
        position = data.findIndex((record) => record.id == id);
        if (position != -1) {
          data.splice(position, 1);
        }
        sessionStorage.setItem('countries', JSON.stringify(data));
      }
      return true;
    } catch (error) {
      return false;
    }
  }
}
