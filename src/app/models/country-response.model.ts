import { CountryModel } from './country.model';

export interface CountryResponseModel {
  list?: Array<CountryModel>;
}
