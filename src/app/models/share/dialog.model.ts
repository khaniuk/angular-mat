import { ActionEnum } from './action-enum';

export interface DialogModel {
  formType: ActionEnum;
  record?: any;
}
