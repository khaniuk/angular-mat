export interface CountryModel {
  id: number;
  name: string;
  topLevelDomain: string;
  alpha2Code: string;
  alpha3Code: string;
  callingCodes: Array<string>;
  capital: string;
  altSpellings: string;
  region: string;
  subregion: string;
  population: number;
  latlng: Array<number>;
  demonym: string;
  area: number;
  gini: number;
  timezones: Array<string>;
  borders: Array<string>;
  nativeName: string;
  numericCode: string;
  currencies: Array<Currency>;
  languages: Array<Language>;
  translations: any;
  flag: string;
  regionalBlocs: any[];
  cioc: string;
  status?: boolean;
}

interface Currency {
  code: string;
  name: string;
  symbol: string;
}

interface Language {
  name: string;
}
