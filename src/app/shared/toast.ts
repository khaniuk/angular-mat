import { MatSnackBar } from '@angular/material/snack-bar';

export class Toast {
  public static open(snackBar: MatSnackBar, message: string, sinClass: string) {
    snackBar.open(message, '', {
      horizontalPosition: 'right',
      verticalPosition: 'top',
      duration: 2000,
      panelClass: [sinClass],
    });
  }
}
