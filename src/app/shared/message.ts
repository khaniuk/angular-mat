export enum MessageEnum {
  MSG_ADD = 'Record added successfully',
  MSG_NOT_ADD = 'Error adding record',
  MSG_UPD = 'Success update record',
  MSG_NOT_UPD = 'Error updating record',
  MSG_DEL = 'Success delete record',
  MSG_NOT_DEL = 'Error deleting record',
}

export enum ToastStyleEnum {
  SUCCESS = 'success-snackbar',
  ERROR = 'error-snackbar',
  WARNING = 'warning-snackbar',
  INFO = 'info-snackbar',
}
